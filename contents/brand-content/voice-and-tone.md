---
name: Voice and tone
---

The following guide outlines the set of standards used for all written company communications to ensure consistency in voice, style, and personality across all of GitLab's public communications.

See [General editorial](https://about.gitlab.com/handbook/marketing/corporate-marketing/#general-editorial-style-guidelines) style guidelines for more.

## About

### GitLab the community

GitLab is an [open source project](https://gitlab.com/gitlab-org/gitlab/) with a large community of contributors. Over 2,000 people worldwide have contributed to GitLab's source code.

### GitLab the company

GitLab Inc. is a company based on the GitLab open source project. GitLab Inc. is an active participant in our community (see our [stewardship of GitLab CE](https://about.gitlab.com/stewardship) for more information), as well as offering GitLab, a product (see below).

### GitLab the product

GitLab is a single application for the entire DevOps lifecycle. See the [product messaging handbook page](https://about.gitlab.com/handbook/marketing/product-marketing/messaging/) for additional information.

## Tone of Voice

The tone of voice we use when speaking as GitLab should always be informed by our [Content Strategy](https://gitlab.com/gitlab-com/marketing/general/blob/master/content/content-strategy.md#strategy). Most importantly, we see our audience as co-conspirators, working together to define and create the next generation of software development practices. The below table should help to clarify further:

| **We are** | **We aren't** |
| --- | --- |
| Equals in our community | Superior |
| Knowledgable | Know-it-alls |
| Empathetic | Patronizing |
| Straightforward | Verbose |
| Irreverent | Disrespectful |
| Playful | Jokey |
| Helpful | Dictatorial |
| Transparent | Opaque |

We explain things in the simplest way possible, using plain, accessible language.

We keep a sense of humor about things, but don't make light of serious issues or problems our users or customers face.

We use colloquialisms and slang, but sparingly (don't look like you're trying too hard!).

We use [inclusive, gender-neutral language](https://litreactor.com/columns/5-easy-ways-to-make-your-writing-gender-neutral).
